function [] = reproject(img, cameraParams, imgPoints, worldPoints)
[worldOrientation,worldLocation,status] = estimateWorldCameraPose(imgPoints,worldPoints,cameraParams);
[R,t] = cameraPoseToExtrinsics(worldOrientation,worldLocation);
projectedPoints  = worldToImage(cameraParams,R,t,worldPoints);
figure
imshow(img);
hold on;
scatter(projectedPoints(:,1), projectedPoints(:,2), 15, [1,0,0], 'filled');
scatter(imgPoints(:,1), imgPoints(:,2), 10, [1,1,1], 'filled');
for p = 1:length(imgPoints)
    pp = projectedPoints(p,:);
    ip = imgPoints(p,:);
    line([pp(1), ip(1)], [pp(2), ip(2)]);
hold off;
end

X = worldPoints;
x = imgPoints;
figure; axis equal;
hold
%text(X(:,1),X(:,2),X(:,3),sprintfc('%d',1:32))

for p = 1:32
    X(p,:,:)
    plot3(X(p,1), X(p,2), X(p,3),'x');
    text(X(p,1), X(p,2), X(p,3), sprintfc('%d = ', p));
end

figure; imshow(img)
hold

for p = 1:32
    x(p,:)
    plot(x(p,1), x(p,2),'x');
    text(x(p,1), x(p,2),sprintfc('%d',p),'Color',[1 1 1]);
end

