import pickle
import argparse
import numpy as np
import cv2
import copy
import random
from collections import OrderedDict

# CAMERA_MATRIX = \
# np.array([[1.405332178851930e+03, 0., 9.474993963285597e+02],
#           [0., 1.397195308832482e+03, 5.225796465456826e+02],
#           [0., 0., 1.]])

CAMERA_MATRIX = \
np.array([[1.41651006e+03, 0.00000000e+00, 9.37622046e+02],
          [0.00000000e+00, 1.40910631e+03, 4.89004911e+02],
          [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

def pickleSave(data, n):
    with open(n+".pkl", "wb") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)         

parser = argparse.ArgumentParser(description='Finds an object pose from 3D-2D point correspondences for the 3 viewpoints pointmatched using printercamcam.py')
#parser.add_argument('pickledData', type=str, help='the pickled data file from printercamcal.py')
parser.add_argument('--pickledMapping', type=str)
parser.add_argument('--pickledModelCoords', type=str)
parser.add_argument('--pickledFrontCoords', type=str)
parser.add_argument('--pickledLeftCoords', type=str)
parser.add_argument('--pickledRightCoords', type=str)

parser.add_argument("frontimg", type=str)
parser.add_argument("leftimg", type=str)
parser.add_argument("rightimg", type=str)

parser.add_argument("--saveForMatlab", action="store_true")

args = parser.parse_args()

pickledMapping = None
pickledModel = None
pickledFront = None
pickledLeft = None
pickledRight = None
modelCoords = None
imgCoordsFront = None
imgCoordsLeft = None
imgCoordsRight = None

if args.pickledMapping != None:
    f = open(args.pickledMapping, "rb")
    pickledMapping = pickle.load(f)
    f.close()

else:
    pickledModel = open(args.pickledModelCoords, "rb")
    pickledFront = open(args.pickledFrontCoords, "rb")
    pickledLeft = open(args.pickledLeftCoords, "rb")
    pickledRight = open(args.pickledRightCoords, "rb")

    modelCoords = pickle.load(pickledModel)
    imgCoordsFront = pickle.load(pickledFront)
    imgCoordsLeft = pickle.load(pickledLeft)
    imgCoordsRight = pickle.load(pickledRight)

    pickledModel.close()
    pickledFront.close()
    pickledLeft.close()
    pickledRight.close()

matchedPoints = OrderedDict()

if pickledMapping:
    matchedPoints = OrderedDict(pickledMapping)
else:
    for c in modelCoords:
        matchedPoints[c] = [imgCoordsFront.pop(0), imgCoordsLeft.pop(0), imgCoordsRight.pop(0)]

filteredPointsFront = copy.deepcopy(matchedPoints)
filteredPointsLeft = copy.deepcopy(matchedPoints)
filteredPointsRight = copy.deepcopy(matchedPoints)

for k, v in matchedPoints.items():
    if None in v[0]:
        del filteredPointsFront[k] # remove points that aren't visible in front image
    if None in v[1]:
        del filteredPointsLeft[k]
    if None in v[2]:
        del filteredPointsRight[k]

#objPoints = np.array(list(matchedPoints.keys()), dtype="float")
objPointsFront = np.array(list(filteredPointsFront.keys()), dtype="float")
objPointsLeft = np.array(list(filteredPointsLeft.keys()), dtype="float")
objPointsRight = np.array(list(filteredPointsRight.keys()), dtype="float")

imgPointsFront = np.array([v[0] for k, v in filteredPointsFront.items()], dtype="float")
imgPointsLeft = np.array([v[1] for k, v in filteredPointsLeft.items()], dtype="float")
imgPointsRight = np.array([v[2] for k, v in filteredPointsRight.items()], dtype="float")

imgPoints = [imgPointsFront, imgPointsLeft, imgPointsRight]

if args.saveForMatlab:
    with open("worldPointsFront.csv", "w") as f:
        for p in objPointsFront:
            f.write("{}, {}, {}\n".format(*p))

    with open("imagePointsFront.csv", "w") as f:
        for p in imgPointsFront:
            f.write("{}, {}\n".format(*p))

    print("Saved CSVs")
    exit()

frontRet, frontRVec, frontTVec = cv2.solvePnP(objPointsFront, imgPointsFront, CAMERA_MATRIX, None)
leftRet, leftRVec, leftTVec = cv2.solvePnP(objPointsLeft, imgPointsLeft, CAMERA_MATRIX, None)
rightRet, rightRVec, rightTVec = cv2.solvePnP(objPointsRight, imgPointsRight, CAMERA_MATRIX, None)

imgs = [cv2.imread(args.frontimg), cv2.imread(args.leftimg), cv2.imread(args.rightimg)]
#plot values to test results
projectedPointsFront, _ = cv2.projectPoints(objPointsFront, frontRVec, frontTVec, CAMERA_MATRIX, None)
projectedPointsLeft, _ = cv2.projectPoints(objPointsLeft, leftRVec, leftTVec, CAMERA_MATRIX, None)
projectedPointsRight, _ = cv2.projectPoints(objPointsRight, rightRVec, rightTVec, CAMERA_MATRIX, None)

projs = [projectedPointsFront, projectedPointsLeft, projectedPointsRight]

colors = [(random.randint(0,255), random.randint(0,255), random.randint(0,255)) for _ in range(300)]

SQ_BLOOM = 2

for i in range(3):
    diffs = [] # keeps track of plotted points for drawing lines
    for idx, p in enumerate(projs[i]):
        coord = p.astype("int").flatten()
        cv2.circle(imgs[i],(coord[0], coord[1]),3,(0,0,255),-1)
        diffs.append((coord[0], coord[1]))

    for idx, p in enumerate(imgPoints[i]):
        coord = p.astype("int").flatten()
        cv2.rectangle(imgs[i],(coord[0]-SQ_BLOOM, coord[1]-SQ_BLOOM),(coord[0]+SQ_BLOOM, coord[1]+SQ_BLOOM),thickness=1,color=colors[idx])
        cv2.line(imgs[i], diffs.pop(0), (coord[0], coord[1]), colors[idx])
        

for i in imgs:
    cv2.imshow("img {}".format(i), i)
    cv2.waitKey(100)

# import code
# code.interact(local=locals())

cv2.imwrite("front_reproject.bmp", imgs[0])
cv2.imwrite("left_reproject.bmp", imgs[1])
cv2.imwrite("right_reproject.bmp", imgs[2])

toSave = [(frontRVec, frontTVec), (leftRVec, leftTVec), (rightRVec, rightTVec), CAMERA_MATRIX]
pickleSave(toSave, "RTCamMatrices")
print("Images and matrices saved")
print("waitkey")
cv2.waitKey(0)
cv2.destroyAllWindows()
