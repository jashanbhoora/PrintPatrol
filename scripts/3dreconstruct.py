import pickle
import argparse
import numpy as np
import cv2
from pyrr import aabb
import stl
from stl import mesh
import copy
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import scipy.io as sio

from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph import Vector
import pyqtgraph.opengl as gl



WHITE = 255
BLACK = 0

#from numpy-stl docs
def findMinsMaxs(obj):
    minx = maxx = miny = maxy = minz = maxz = None
    for p in obj.points:
        # p contains (x, y, z)
        if minx is None:
            minx = p[stl.Dimension.X]
            maxx = p[stl.Dimension.X]
            miny = p[stl.Dimension.Y]
            maxy = p[stl.Dimension.Y]
            minz = p[stl.Dimension.Z]
            maxz = p[stl.Dimension.Z]
        else:
            maxx = max(p[stl.Dimension.X], maxx)
            minx = min(p[stl.Dimension.X], minx)
            maxy = max(p[stl.Dimension.Y], maxy)
            miny = min(p[stl.Dimension.Y], miny)
            maxz = max(p[stl.Dimension.Z], maxz)
            minz = min(p[stl.Dimension.Z], minz)
    return minx, maxx, miny, maxy, minz, maxz

parser = argparse.ArgumentParser(description='Reconstructs a 3D volume from images of an STL')
parser.add_argument('--silhouettes', type=str, required=True)
parser.add_argument('--stl', type=str, required=True)
parser.add_argument('--matrices', type=str, required=True)
parser.add_argument('--outname', type=str, required=True)
parser.add_argument('--gridsize', type=int, help="resolution of the voxel grid", default=100)
parser.add_argument('--rotx', type=float, help='Rotation around X axis', default=0)
parser.add_argument('--roty', type=float, help='Rotation around Y axis', default=0)
parser.add_argument('--rotz', type=float, help='Rotation around Z axis', default=0)
parser.add_argument('--scale', type=float, help='Apply a scaling factor to the STL in all axes (as a percentage)', default=100)
parser.add_argument('--showindiv', action="store_true")
args = parser.parse_args()

f = open(args.silhouettes, "rb")
silhouettes = pickle.load(f)
f.close()

#load mesh and matrices
shapeMesh = mesh.Mesh.from_file(args.stl)

#scale mesh
factor = args.scale/100
shapeMesh.x *= factor
shapeMesh.y *= factor
shapeMesh.z *= factor

shiftedMesh = copy.deepcopy(shapeMesh)
matrices = None
with open(args.matrices, "rb") as f:
    matrices = pickle.load(f)

CAMERA_MATRIX = matrices[3]

# center mesh as in slic3r
# Calc AABB
bb = aabb.create_from_points(shiftedMesh)
aabbCenter = aabb.centre_point(bb)

#apply any rotations
shiftedMesh.rotate([0.5, 0, 0], np.deg2rad(args.rotx), aabbCenter[0:3])
shiftedMesh.rotate([0, 0.5, 0], np.deg2rad(args.roty), aabbCenter[0:3])
shiftedMesh.rotate([0, 0, 0.5], np.deg2rad(args.rotz), aabbCenter[0:3])

#translate mesh as in slic3r
shiftedMesh.x -= aabbCenter[0]
shiftedMesh.y -= aabbCenter[1]
shiftedMesh.z -= shiftedMesh.min_[2] #the lowest z value in the mesh

#move the original stl so that its corner is in the origin like the volume
shapeMesh.x -= shapeMesh.min_[0]
shapeMesh.y -= shapeMesh.min_[1]
shapeMesh.z -= shapeMesh.min_[2] 
#scale mesh we are going to plot on volume to match volume grid size
#TODO check this is correct
shapeMesh.x *= args.gridsize/shapeMesh.max_[0]
shapeMesh.y *= args.gridsize/shapeMesh.max_[1]
shapeMesh.z *= args.gridsize/shapeMesh.max_[2]


voxelGridBool = np.ones((args.gridsize,args.gridsize,args.gridsize), dtype=bool)
#4th dimension represents colour of the voxel for pyqtgraph
voxelGrid = np.full((args.gridsize,args.gridsize,args.gridsize, 4), [127,127,127,100], dtype="uint8")

mms = findMinsMaxs(shiftedMesh)

xs = np.linspace(mms[0], mms[1], args.gridsize)
ys = np.linspace(mms[2], mms[3], args.gridsize)
zs = np.linspace(mms[4], mms[5], args.gridsize) 

xsl, ysl, zsl = xs.tolist(), ys.tolist(), zs.tolist() # for reverse lookups using index

expectedSils = silhouettes[0]
actualSils = silhouettes[1]

for idx in range(3):
    r, t = matrices[idx]

    for x in xs:
        for y in ys:
            for z in zs:
                projectedPoint = cv2.projectPoints(np.array([[x,y,z]]), r, t, CAMERA_MATRIX, None)[0].flatten()
                try:
                    _x, _y, _z = xsl.index(x), ysl.index(y), zsl.index(z)
                    if actualSils[idx][int(projectedPoint[1]), int(projectedPoint[0])] == BLACK: # if not in silhouette
                        voxelGridBool[_x, _y, _z] = False
                        voxelGrid[_x, _y, _z] = [0,0,0,0]
                    else:
                        voxelGrid[_x, _y, _z] = [60,0,0,100] # colour this voxel for the render

                except Exception as e:
                    print(projectedPoint)
                    print(e)
                    # import code
                    # code.interact(local=locals())

                

# fig = vp.Fig(bgcolor='k', size=(800, 800), show=False)

# clim = [0, 1]
# vol_pw = fig[0, 0]
# vol_pw.volume(voxelGrid, clim=clim, cmap='hsl')
# vol_pw.view.camera.elevation = 30
# vol_pw.view.camera.azimuth = 30
# vol_pw.view.camera.scale_factor /= 1.5

# shape = voxelGrid.shape
# fig[1, 0].image(voxelGrid[:, :, shape[2] // 2], cmap='grays', clim=clim,
#                 fg_color=(0.5, 0.5, 0.5, 1))
# fig[0, 1].image(voxelGrid[:, shape[1] // 2, :], cmap='grays', clim=clim,
#                 fg_color=(0.5, 0.5, 0.5, 1))
# fig[1, 1].image(voxelGrid[shape[0] // 2, :, :].T, cmap='grays', clim=clim,
#                 fg_color=(0.5, 0.5, 0.5, 1))
#fig.show(run=True)

# verify volume by checking how many voxels are contained within the STL
# STL isn't a volume, so can't do this directly..
# idea is to compute the normal for every point in every triangle, and project that normal in both directions
# using ray tracing/winding rule, we can know which direction is inside (if any

name = "{}.mat".format(args.outname)
print("saving " + name)
sio.savemat(name, {args.outname: voxelGridBool})
print("Finished processing")



# create qtgui
app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.orbit(256, 256) 
camCenter = aabb.centre_point(aabb.create_from_points(shapeMesh))
w.opts['center'] = Vector(*camCenter[0:3])
w.opts['distance'] = 200
w.show()
w.setWindowTitle('3D Reconstruction')

# g = gl.GLGridItem()
# g.scale(20, 20, 1)
# w.addItem(g)

o = gl.GLScatterPlotItem(pos=np.array([0,0,0]), color=[255,255,255,255], size=10, pxMode=True)
w.addItem(o)

m = gl.GLMeshItem(vertexes=shapeMesh.vectors, drawEdges=True, drawFaces=True, smooth=True)
w.addItem(m)

v = gl.GLVolumeItem(voxelGrid, sliceDensity=3, smooth=False, glOptions='additive')
w.addItem(v)

if args.showindiv:
    m2 = gl.GLMeshItem(vertexes=shapeMesh.vectors, drawEdges=True, drawFaces=False)
    m2.translate(-(shapeMesh.max_[0]-shapeMesh.min_[0])*3,0,0)
    w.addItem(m2)

    v2 = gl.GLVolumeItem(voxelGrid, sliceDensity=1, smooth=False, glOptions='translucent')
    v2.translate((shapeMesh.max_[0]-shapeMesh.min_[0])*3, 0, 0)
    w.addItem(v2)

axis = gl.GLAxisItem()
axis.setSize(x=100,y=100,z=100)
w.addItem(axis)
QtGui.QApplication.instance().exec_()



# fig = plt.figure()
# ax = mplot3d.Axes3D(fig)
# ax.scatter(0, 0, 0, color="black", marker="D")

# # plt may be moving the voxels to positive when plotting, even though values are negative
# ax.voxels(voxelGridBool, facecolors='#1f77b430', edgecolor='black')
# plt.show()