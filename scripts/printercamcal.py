#!/usr/bin/env python3

import numpy as np
import cv2
from pyrr import aabb
from stl import mesh
from mpl_toolkits import mplot3d
from matplotlib import pyplot, colors
import argparse
import subprocess
import copy
import pickle

# TODO improve argument format
# TODO add scaling transforms?

parser = argparse.ArgumentParser(description='Performs calibration routines for Print Patrol (assumes 3 Logitech C920 webcam)')
parser.add_argument('stl', type=str, help='The mesh to process')
parser.add_argument('frontcam', type=int, help='The index of the front camera')
parser.add_argument('leftcam', type=int, help='The index of the left camera')
parser.add_argument('rightcam', type=int, help='The index of the right camera')
parser.add_argument('outfile', type=str, help='Name of the output file')
parser.add_argument('--rotx', type=float, help='Rotation around X axis in degrees', default=0)
parser.add_argument('--roty', type=float, help='Rotation around Y axis in degrees', default=0)
parser.add_argument('--rotz', type=float, help='Rotation around Z axis in degrees', default=0)
parser.add_argument('--scale', type=float, help='Apply a scaling factor in all axes (as a percentage)', default=100)

subparsers = parser.add_subparsers(dest='subcommand')

onlineSubparser = subparsers.add_parser("online")
offlineSubparser = subparsers.add_parser("offline")

onlineSubparser.add_argument('--show', action='store_true', help='Show each camera feed, then abort')
onlineSubparser.add_argument('--capture', action='store_true', help='Save image from each camera')

offlineSubparser.add_argument("frontimg", type=str, help="The front image path")
offlineSubparser.add_argument("leftimg", type=str, help="The left image path")
offlineSubparser.add_argument("rightimg", type=str, help="The right image path")


args = parser.parse_args()

if args.subcommand not in ["online", "offline"]:
    print("Mode not specified (online or offline)")
    exit(1)

LEFT = 1
MIDDLE = 2
RIGHT = 3
FOCUS = 30

FRONTCAM = args.frontcam # camera facing the front of the printer
LEFTCAM = args.leftcam # first camera moving clockwise from the front camera around the printer
RIGHTCAM = args.rightcam # the remaining camera
CAMNAMES = {FRONTCAM: "front", LEFTCAM: "left", RIGHTCAM: "right"} # just for printing

modelCoords = []

imgCoordsFront = []
imgCoordsLeft = []
imgCoordsRight = []

calibImgs = {}
shapeMesh = mesh.Mesh.from_file(args.stl)

DELAY = 200

def pickleSave(data, viewName):
    with open(viewName+"_"+args.outfile, "wb") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def onPick(event):
    global modelCoords

    xs, ys, zs = event.artist._offsets3d
    x, y, z = round(xs[event.ind[0]], 3), round(ys[event.ind[0]], 3), round(zs[event.ind][0],  3)
    ax = pyplot.gca()
    ax.text(x, y, z, str(len(modelCoords)), fontdict=None, withdash=False, fontsize=10, backgroundcolor='w')
    modelCoords.append((x, y, z))

    pyplot.draw()
    print("picked from model: ({}, {}, {})".format(x, y, z))

def eventCallbackFront(event, x, y, flags, param):
    #update zoom window
    if event == cv2.EVENT_MOUSEMOVE:
        try:
            zoom = calibImgs[FRONTCAM][y-20:y+20, x-20:x+20].copy()
            h, w, _ = zoom.shape
            zoom[int(h/2), int(w/2)] = (0, 0, 255)
            cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("zoom", 500, 500)
            cv2.imshow("zoom", zoom)
            cv2.waitKey(1)
            return
        except:
            return

    #respond to click
    global imgCoordsFront
    toShow = calibImgs[FRONTCAM].copy()
    if event == cv2.EVENT_LBUTTONDOWN:
        imgCoordsFront.append((x, y))
        print("picked from front image ({}, {})".format(x, y))

    elif event == cv2.EVENT_RBUTTONDOWN:
        imgCoordsFront.append((None, len(imgCoordsFront))) #use y to represent point number that is invisible
        print("point {} marked as not visible in front img".format(len(imgCoordsFront)))

    #redraw image with markings
    noneCoords = [x for x in imgCoordsFront if x[0] is None]
    coords = [x for x in imgCoordsFront if x[0] is not None]
    for c in coords:
            cv2.putText(toShow, "{}".format(imgCoordsFront.index(c)), (c[0], c[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.circle(toShow, (c[0], c[1]), 2, (0, 255, 0), -1)

    for c in noneCoords:
        cv2.putText(toShow, "{},".format(c[1]), (10+noneCoords.index(c)*50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("front img", toShow)
    cv2.waitKey(1)

def eventCallbackRight(event, x, y, flags, param):
    #update zoom window
    if event == cv2.EVENT_MOUSEMOVE:
        try:
            zoom = calibImgs[RIGHTCAM][y-20:y+20, x-20:x+20].copy()
            h, w, _ = zoom.shape
            zoom[int(h/2), int(w/2)] = (0, 0, 255)
            cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("zoom", 500, 500)
            cv2.imshow("zoom", zoom)
            cv2.waitKey(1)
            return
        except:
            return

    #respond to click
    global imgCoordsRight
    toShow = calibImgs[RIGHTCAM].copy()
    if event == cv2.EVENT_LBUTTONDOWN:
        imgCoordsRight.append((x, y))
        print("picked from Right image ({}, {})".format(x, y))

    elif event == cv2.EVENT_RBUTTONDOWN:
        imgCoordsRight.append((None, len(imgCoordsRight))) #use y to represent point number that is invisible
        print("point {} marked as not visible in right img".format(len(imgCoordsRight)))

    #redraw image with markings
    noneCoords = [x for x in imgCoordsRight if x[0] is None]
    coords = [x for x in imgCoordsRight if x[0] is not None]

    for c in coords:
            cv2.putText(toShow, "{}".format(imgCoordsRight.index(c)), (c[0], c[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.circle(toShow, (c[0], c[1]), 2, (0, 255, 0), -1)

    for c in noneCoords:
        cv2.putText(toShow, "{},".format(c[1]), (10+noneCoords.index(c)*50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("right img", toShow)
    cv2.waitKey(1)

def eventCallbackLeft(event, x, y, flags, param):
    #update zoom window
    if event == cv2.EVENT_MOUSEMOVE:
        try:
            zoom = calibImgs[LEFTCAM][y-20:y+20, x-20:x+20].copy()
            h, w, _ = zoom.shape
            zoom[int(h/2), int(w/2)] = (0, 0, 255)
            cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("zoom", 500, 500)
            cv2.imshow("zoom", zoom)
            cv2.waitKey(1)
            return
        except:
            return

    #respond to click
    global imgCoordsLeft
    toShow = calibImgs[LEFTCAM].copy()
    if event == cv2.EVENT_LBUTTONDOWN:
        imgCoordsLeft.append((x, y))
        print("picked from left image ({}, {})".format(x, y))

    elif event == cv2.EVENT_RBUTTONDOWN:
        imgCoordsLeft.append((None, len(imgCoordsLeft))) #use y to represent point number that is invisible
        print("point {} marked as not visible in left img".format(len(imgCoordsLeft)))

    #redraw image with markings
    noneCoords = [x for x in imgCoordsLeft if x[0] is None]
    coords = [x for x in imgCoordsLeft if x[0] is not None]
    for c in coords:
        cv2.putText(toShow, "{}".format(imgCoordsLeft.index(c)), (c[0], c[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        cv2.circle(toShow, (c[0], c[1]), 2, (0, 255, 0), -1)

    for c in noneCoords:
        cv2.putText(toShow, "{},".format(c[1]), (10+noneCoords.index(c)*50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("left img", toShow)
    cv2.waitKey(1)

if args.subcommand == "online":
    # TODO use v4l python bindings instead
    print("Disabling autofocus and setting focus to {} on first 3 video devices".format(FOCUS)) # note that the system needs to have v4l-utils installed for this
    for c in [0, 1, 2]:
        subprocess.call(["v4l2-ctl -d {} -c focus_auto=0 --verbose".format(c)], shell=True)
        subprocess.call(["v4l2-ctl -d {} -c focus_absolute={} --verbose".format(c, FOCUS)], shell=True)

    if args.show:
        for c in [FRONTCAM, LEFTCAM, RIGHTCAM]:
            cap = cv2.VideoCapture(c)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
            while True:
                # Capture frame-by-frame
                ret, frame = cap.read()
                cv2.imshow('frame', frame)
                pressed = cv2.waitKey(1) & 0xFF
                if pressed == ord('q'):
                    if args.capture:
                        cv2.imwrite("capture_{}.bmp".format(CAMNAMES[c]), frame)
                    break
            # When everything done, release the capture
            cap.release()
            cv2.destroyAllWindows()
        exit()

    # check 3 cameras
    if FRONTCAM == None and LEFTCAM == None and RIGHTCAM == None:
        try:
            # TODO infer third camera
            for c in [0, 1, 2]:
                cam = cv2.VideoCapture(c)
                cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                cv2.waitKey(DELAY) # allow camera to open
                ret, checkImg = cam.read()
                cv2.imshow("Check Image {}".format(c), checkImg)
                cv2.waitKey(DELAY)
                del cam
                idx = int(input("Enter camera index (Front = 0, Left = 1, Right = 2): "))
                cv2.destroyAllWindows()

                if idx == 0:
                    assert LEFTCAM != c
                    assert RIGHTCAM != c
                    FRONTCAM = c
                elif idx == 1:
                    assert FRONTCAM != c
                    assert RIGHTCAM != c
                    LEFTCAM = c
                elif idx == 2:
                    assert FRONTCAM != c
                    assert LEFTCAM != c
                    RIGHTCAM = c
                else:
                    raise Exception("Invalid index {} entered".format(idx))

        except Exception as e:
            print("Unknown Exception when initialising cameras. 3 cameras may not be detected?")
            print(e)
            exit(1)

    assert FRONTCAM != None
    assert LEFTCAM != None
    assert RIGHTCAM != None

    print("python3 printercamcal.py --frontcam={} --leftcam={} --rightcam={}".format(FRONTCAM, LEFTCAM, RIGHTCAM))

    # take calibration images
    for c in [FRONTCAM, LEFTCAM, RIGHTCAM]:
        cam = cv2.VideoCapture(c)
        cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        cv2.waitKey(DELAY) # allow camera to open
        ret, calibImg = cam.read()
        del cam # close camera
        cv2.waitKey(DELAY)
        cv2.imwrite("calib_{}.bmp".format(CAMNAMES[c]), calibImg)
        calibImgs[c] = calibImg.copy()
        # have images of printed object, now do point matching with STL of the model

elif args.subcommand == "offline":
    calibImgs[FRONTCAM] = cv2.imread(args.frontimg)
    calibImgs[LEFTCAM] = cv2.imread(args.leftimg)
    calibImgs[RIGHTCAM] = cv2.imread(args.rightimg)

else:
    print("Unknown subcommand")
    exit(1)

figure = pyplot.figure()
axes = mplot3d.Axes3D(figure)
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_zlabel('z')
axes.scatter(0, 0, 0, c="black", marker="D") # Plot origin

#scale mesh
factor = args.scale/100
shapeMesh.x *= factor
shapeMesh.y *= factor
shapeMesh.z *= factor

# Calc AABB
bb = aabb.create_from_points(shapeMesh)
aabbCenter = aabb.centre_point(bb)
axes.scatter(aabbCenter[0], aabbCenter[1], aabbCenter[2], c=colors.to_rgba("r", 0.75)) # AABB center point on original mesh

#apply any rotations
shapeMesh.rotate([0.5, 0, 0], np.deg2rad(args.rotx), aabbCenter[0:3])
shapeMesh.rotate([0, 0.5, 0], np.deg2rad(args.roty), aabbCenter[0:3])
shapeMesh.rotate([0, 0, 0.5], np.deg2rad(args.rotz), aabbCenter[0:3])

#copy mesh and translate it so that AABB origin is on system origin and object is on bed
shiftedMesh = copy.deepcopy(shapeMesh)
shiftedMesh.x -= aabbCenter[0]
shiftedMesh.y -= aabbCenter[1]
shiftedMesh.z -= shapeMesh.min_[2] #the lowest z value in the mesh

shiftedCollection = mplot3d.art3d.Poly3DCollection(shiftedMesh.vectors, alpha=0.5)
shiftedCollection.set_facecolor([0.5, 0.5, 0.5])
axes.add_collection3d(shiftedCollection)

#split coordinates of mesh into dimensions then
#remerge as coordinate tuples, then remove duplicates to get set of unique corners
sflatCoords = shiftedMesh.vectors.flatten()
sxs, sys, szs = sflatCoords[0::3], sflatCoords[1::3], sflatCoords[2::3]
corners = set(zip(sxs, sys, szs))
axes.plot(sxs, sys, szs, c=colors.to_rgba("b", 0.2)) # plot lines of mesh translucently
axes.scatter([c[0] for c in corners], [c[1] for c in corners], [c[2] for c in corners], c="r", picker=True)

# Auto scale to the mesh size
scale = shiftedMesh.vectors.flatten(-1)
axes.auto_scale_xyz(scale, scale, scale)

cid = figure.canvas.mpl_connect('pick_event', onPick)

#match corners between model and images in same order
cv2.namedWindow("front img")
cv2.namedWindow("left img")
cv2.namedWindow("right img")
cv2.setMouseCallback("front img", eventCallbackFront)
cv2.setMouseCallback("right img", eventCallbackRight)
cv2.setMouseCallback("left img", eventCallbackLeft)

cv2.imshow("front img", calibImgs[FRONTCAM])
cv2.imshow("left img", calibImgs[LEFTCAM])
cv2.imshow("right img", calibImgs[RIGHTCAM])
cv2.waitKey(1)
pyplot.show()

pickleSave(modelCoords, "model")
pickleSave(imgCoordsFront, "front")
pickleSave(imgCoordsLeft, "left")
pickleSave(imgCoordsRight, "right")
print("Individual pkl files saved")


mapping = {}
for c in modelCoords:
    mapping[c] = [imgCoordsFront.pop(0), imgCoordsLeft.pop(0), imgCoordsRight.pop(0)]

assert len(mapping) == len(modelCoords)

if len(mapping):
    pickleSave(mapping, "mapping")
    print(mapping)
else:
    print("Mapping not saved")

# TODO need undo functions for marking images
