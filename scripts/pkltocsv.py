import sys
import pickle

f = open(sys.argv[1], "rb")
pkl = pickle.load(f)
f.close()

worldCsv = open("worldPoints.csv", "w")
imgCsv = open("imgPoints.csv", "w")

for k,v in pkl.items():
    worldCsv.write("{}, {}, {}\n".format(*k))
    imgCsv.write("{}, {}\n".format(*v))

worldCsv.close()
imgCsv.close()

# import code
# code.interact(local=locals())