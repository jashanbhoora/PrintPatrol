import numpy as np
import cv2

cap = cv2.VideoCapture(0)
cap2 = cv2.VideoCapture(1)
#cap.set(4, 1080)
#cap.set(3, 1920)
#cap2.set(4, 1080)
#cap2.set(3, 1920)

cams = [cap,cap2]

while(True):
    for c in cams:
        if c.isOpened():
            ret, img = c.read()
            #print("read")
            #cv2.imshow(str(c),img)
            #print("show")
            cv2.waitKey(500)
            
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            
            # find Harris corners
            gray = np.float32(gray)
            #dst = cv2.cornerHarris(gray,6,5,0.24)
            dst = cv2.cornerHarris(gray,7,5,0.04)
            #cv2.imshow("dst", dst)
            dst = cv2.dilate(dst,None)
            #cv2.imshow("dst2", dst)
            ret, dst = cv2.threshold(dst,0.01*dst.max(),255,0)
            dst = np.uint8(dst)

            #cv2.imshow("dst3", dst)   

            # find centroids
            ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)

            # define the criteria to stop and refine the corners
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
            corners = cv2.cornerSubPix(gray,np.float32(centroids),(5,5),(-1,-1),criteria)

            # Now draw them
            res = np.hstack((centroids,corners))
            res = np.int0(res)
            #print(res)
            try:
                img[res[:,1],res[:,0]]=[0,0,255]
                img[res[:,3],res[:,2]] = [0,255,0]

            except IndexError as e:
                print("    " + str(e))

            #cv2.imwrite('subpixel5.png',img)

            cv2.imshow(str(c),img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

cap.release()
cap2.release()
cv2.destroyAllWindows()