#!/usr/bin/env python3

import numpy as np
from stl import mesh
from mpl_toolkits import mplot3d
from matplotlib import pyplot, colors
import cv2
from bidict import bidict

import pdb, traceback, sys

LEFT = 1
MIDDLE = 2
RIGHT = 3

USESAVED = True
CAMTEST = False
DELAY = 20
SAVEPAIRS = True
SHOWIMGS = False

modelToImagePairsLeft = bidict()
modelToImagePairsRight = bidict()
modelCoords = []
imageCoordsLeft = []
imageCoordsRight = []

def onPick(event):
    global modelCoords

    xs, ys, zs = event.artist._offsets3d
    x, y, z = round(xs[event.ind[0]],3), round(ys[event.ind[0]],3), round(zs[event.ind][0],3)
    ax = pyplot.gca()
    ax.text(x,y,z, str(len(modelCoords)), fontdict=None, withdash=False, fontsize=10, backgroundcolor='w')  
    modelCoords.append((x,y,z))

    pyplot.draw()
    print("picked from model: ({0}, {1}, {2})".format(x,y,z)) 

def eventCallbackRight(event,x,y,flags,param):
    if event == cv2.EVENT_MOUSEMOVE:
        zoom = imgr[y-20:y+20, x-20:x+20].copy()
        h,w, _ = zoom.shape
        zoom[int(h/2),int(w/2)] = (0,0,255)
        cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
        cv2.resizeWindow("zoom",500,500)
        cv2.imshow("zoom", zoom)
    
    if event == cv2.EVENT_LBUTTONDOWN:
        global imageCoordsRight
        cv2.putText(imgr,"{0}".format(len(imageCoordsRight)), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0),2)
        cv2.circle(imgr,(x,y),2,(0,0,0),-1)
        imageCoordsRight.append((x,y))
        cv2.imshow("imgr", imgr)
        print("picked from right image ({0}, {1})".format(x,y))

def eventCallbackLeft(event,x,y,flags,param):
    if event == cv2.EVENT_MOUSEMOVE:
        zoom = imgl[y-20:y+20, x-20:x+20].copy()
        h,w, _ = zoom.shape
        cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
        cv2.resizeWindow("zoom",500,500)
        zoom[int(h/2),int(w/2)] = (0,0,255)
        cv2.imshow("zoom", zoom)

    if event == cv2.EVENT_LBUTTONDOWN:
        global imageCoordsLeft
        cv2.putText(imgl,"{0}".format(len(imageCoordsLeft)), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0),2)
        cv2.circle(imgl,(x,y),2,(0,0,0),-1)
        imageCoordsLeft.append((x,y))
        cv2.imshow("imgl", imgl)
        print("picked from left image ({0}, {1})".format(x,y))

#shapeMesh = mesh.Mesh.from_file("../models/STLs/stepsscaled150positioned.stl")
shapeMesh = mesh.Mesh.from_file("../models/STLs/steps.stl")
imgl = None
imgr = None

if USESAVED:
    imgl = cv2.imread("imgl.bmp")
    imgr = cv2.imread("imgr.bmp")
else:

    c0 = cv2.VideoCapture(1)
    c0.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    c0.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    cv2.waitKey(DELAY)
    if CAMTEST:
        c0.set(37, 1)
        while True:
            # Capture frame-by-frame
            ret, frame = c0.read()
            # Our operations on the frame come here
            #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # Display the resulting frame
            cv2.imshow('imgl test', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    #imgl = cv2.cvtColor(c0.read()[1], cv2.COLOR_BGR2RGB)
    imgl = c0.read()[1]
    cv2.imwrite("imgl.bmp", imgl)
    if SHOWIMGS:
        cv2.imshow("imgl", imgl)
        cv2.waitKey(DELAY)
    del c0

    c1 = cv2.VideoCapture(0)
    c1.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    c1.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    cv2.waitKey(DELAY)
    if CAMTEST:
        c1.set(37, 1)
        while True:
            # Capture frame-by-frame
            ret, frame = c1.read()
            # Our operations on the frame come here
            # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # Display the resulting frame
            cv2.imshow('imgl test', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    #imgr = cv2.cvtColor(c1.read()[1], cv2.COLOR_BGR2RGB)
    imgr = c1.read()[1]
    cv2.imwrite("imgr.bmp", imgr)
    if SHOWIMGS:
        cv2.imshow("imgr", imgr)
        cv2.waitKey(DELAY)
    del c1

figure = pyplot.figure()
axes = mplot3d.Axes3D(figure)
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_zlabel('z')
axes.add_collection3d(mplot3d.art3d.Poly3DCollection(shapeMesh.vectors))

#split coordinates of mesh into dimensions then
#remerge as coordinate tuples, then remove duplicates to get set of unique corners
flatCoords = shapeMesh.vectors.flatten()
xs, ys, zs = flatCoords[0::3], flatCoords[1::3], flatCoords[2::3]
corners = set(zip(xs, ys, zs))

axes.plot(xs, ys, zs, c=colors.to_rgba("b", 0.2)) # plot lines of mesh translucently
axes.scatter([c[0] for c in corners], [c[1] for c in corners], [c[2] for c in corners], c="r", picker=True)

# Auto scale to the mesh size
scale = shapeMesh.points.flatten(-1)
axes.auto_scale_xyz(scale, scale, scale)

cid = figure.canvas.mpl_connect('pick_event', onPick)

# Show the plot to the screen
axes.scatter(0,0,0, c="black", marker="D")

#match corners between model and images in same order
cv2.namedWindow("imgl")
cv2.namedWindow("imgr")
cv2.setMouseCallback("imgr", eventCallbackRight)
cv2.setMouseCallback("imgl", eventCallbackLeft)

cv2.imshow("imgl", imgl)
cv2.imshow("imgr", imgr)
pyplot.show()

assert len(imageCoordsLeft) == len(imageCoordsRight) == len(modelCoords)

for mc in modelCoords:
    modelToImagePairsLeft[mc] = imageCoordsLeft.pop()

for mc in modelCoords:
    modelToImagePairsRight[mc] = imageCoordsRight.pop()

assert len(modelToImagePairsLeft) == len(modelToImagePairsRight) == len(modelCoords)

# import code
# code.interact(local=locals())

if(SAVEPAIRS):
    f = open("modeltoimagepairs.txt", "w")
    f.write("{0}\n".format(len(modelCoords)))
    for k in modelToImagePairsLeft:
        f.write("{0}:{1}\n".format(k,modelToImagePairsLeft[k]))
    f.write("\n")
    for k in modelToImagePairsRight:
        f.write("{0}:{1}\n".format(k,modelToImagePairsRight[k]))
    f.close()
    print("Wrote {0} pairs to file".format(len(modelCoords)))
