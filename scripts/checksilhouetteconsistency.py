import pickle
import argparse
import numpy as np
import cv2

parser = argparse.ArgumentParser(description='Checks the silhouettes from projectsilhouette.py for consistency')
parser.add_argument('pickledSilhouettes', type=str, help="Path to the pickled silhouettes")
args = parser.parse_args()
f = open(args.pickledSilhouettes, "rb")
silhouettes = pickle.load(f)
f.close()

expectedSils = silhouettes[0]
actualSils = silhouettes[1]

for idx in range(len(expectedSils)):
    expected = expectedSils[idx]
    actual = actualSils[idx]
    assert expected.shape == actual.shape # should hold because the same bboxes were used earlier

    acc = 0
    for w in range(expected.shape[0]):
        for h in range(expected.shape[1]):
            if expected[w,h] == actual[w,h]:
                acc += 1

    print("View {} silhouette consistency: {}%".format(idx, acc/expected.size*100))
    cv2.imshow("actual{}".format(idx), actual)
    cv2.imshow("expected{}".format(idx),expected)
    
cv2.waitKey(0)
import code
code.interact(local=locals())

