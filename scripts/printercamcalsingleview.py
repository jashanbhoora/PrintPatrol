#!/usr/bin/env python3

import numpy as np
import cv2
from pyrr import aabb
from stl import mesh
from mpl_toolkits import mplot3d
from matplotlib import pyplot, colors
import argparse
import subprocess
import copy
import pickle

# TODO improve argument format
# TODO add scaling transforms?

parser = argparse.ArgumentParser(description='Performs calibration routines for one view in Print Patrol (assumes 1 Logitech C920 webcam)')
parser.add_argument('stl', type=str, help='The mesh to process')
parser.add_argument('outfile', type=str, help='Name of the output file')
parser.add_argument('--rotx', type=float, help='Rotation around X axis in degrees', default=0)
parser.add_argument('--roty', type=float, help='Rotation around Y axis in degrees', default=0)
parser.add_argument('--rotz', type=float, help='Rotation around Z axis in degrees', default=0)

subparsers = parser.add_subparsers(dest='subcommand')

onlineSubparser = subparsers.add_parser("online")
offlineSubparser = subparsers.add_parser("offline")

onlineSubparser.add_argument('--show', action='store_true', help='Show each camera feed, then abort')

offlineSubparser.add_argument("img", type=str, help="The image path")


args = parser.parse_args()

if args.subcommand not in ["online", "offline"]:
    print("Mode not specified (online or offline)")
    exit(1)

LEFT = 1
MIDDLE = 2
RIGHT = 3
FOCUS = 30

tempFocus = [0, 0, 0] # to store values while cameras are being focused

calibImg = None
imgCoords = []
modelCoords = []

shapeMesh = mesh.Mesh.from_file(args.stl)

DELAY = 200

def pickleSave(data, viewName):
    with open(viewName+"_"+args.outfile, "wb") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def onPick(event):
    global modelCoords

    xs, ys, zs = event.artist._offsets3d
    x, y, z = round(xs[event.ind[0]], 3), round(ys[event.ind[0]], 3), round(zs[event.ind][0],  3)
    ax = pyplot.gca()
    ax.text(x, y, z, str(len(modelCoords)), fontdict=None, withdash=False, fontsize=10, backgroundcolor='w')
    modelCoords.append((x, y, z))

    pyplot.draw()
    print("picked from model: ({}, {}, {})".format(x, y, z))

def eventCallback(event, x, y, flags, param):
    #update zoom window
    if event == cv2.EVENT_MOUSEMOVE:
        try:
            zoom = calibImg[y-20:y+20, x-20:x+20].copy()
            h, w, _ = zoom.shape
            zoom[int(h/2), int(w/2)] = (0, 0, 255)
            cv2.namedWindow("zoom", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("zoom", 500, 500)
            cv2.imshow("zoom", zoom)
            cv2.waitKey(1)
            return
        except:
            return

    #respond to click
    global imgCoords
    toShow = calibImg.copy()
    if event == cv2.EVENT_LBUTTONDOWN:
        imgCoords.append((x, y))
        print("picked from image ({}, {})".format(x, y))

    elif event == cv2.EVENT_RBUTTONDOWN:
        imgCoords.append((None, len(imgCoords))) #use y to represent point number that is invisible
        print("point {} marked as not visible in img".format(len(imgCoords)))

    #redraw image with markings
    noneCoords = [x for x in imgCoords if x[0] is None]
    coords = [x for x in imgCoords if x[0] is not None]
    for c in coords:    
        cv2.putText(toShow, "{}".format(imgCoords.index(c)), (c[0], c[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        cv2.circle(toShow, (c[0], c[1]), 2, (0, 255, 0), -1)  

    for c in noneCoords:
        cv2.putText(toShow, "{},".format(c[1]), (10+noneCoords.index(c)*50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
    
    cv2.imshow("img", toShow)
    cv2.waitKey(1)      

if args.subcommand == "online":
    # TODO use v4l python bindings instead
    print("Disabling autofocus and setting focus to {} on first 3 video devices".format(FOCUS)) # note that the system needs to have v4l-utils installed for this
    subprocess.call(["v4l2-ctl -d 0 -c focus_auto=0 --verbose"], shell=True)
    subprocess.call(["v4l2-ctl -d 0 -c focus_absolute={} --verbose".format(FOCUS)], shell=True)

    if args.show:
        for c in [0]:
            cap = cv2.VideoCapture(c)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
            while True:
                # Capture frame-by-frame
                ret, frame = cap.read()
                cv2.imshow('frame', frame)
                pressed = cv2.waitKey(1) & 0xFF
                if pressed == ord('q'):
                    break
            # When everything done, release the capture
            cap.release()
            cv2.destroyAllWindows()
        exit()

    # take calibration images
    for c in [0]:
        cam = cv2.VideoCapture(c)
        cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        cv2.waitKey(DELAY) # allow camera to open
        ret, calibImg = cam.read()
        del cam # close camera
        cv2.waitKey(DELAY)
        cv2.imwrite("calib_img.bmp", calibImg)
        calibImg = calibImg.copy()
        # have images of printed object, now do point matching with STL of the model

elif args.subcommand == "offline":
    calibImg = cv2.imread(args.img)

else:
    print("Unknown subcommand")
    exit(1)

figure = pyplot.figure()
axes = mplot3d.Axes3D(figure)
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_zlabel('z')
axes.scatter(0, 0, 0, c="black", marker="D") # Plot origin

# Calc AABB
bb = aabb.create_from_points(shapeMesh)
aabbCenter = aabb.centre_point(bb)
axes.scatter(aabbCenter[0], aabbCenter[1], aabbCenter[2], c=colors.to_rgba("r", 0.75)) # AABB center point on original mesh

#apply any rotations
shapeMesh.rotate([0.5, 0, 0], np.deg2rad(args.rotx), aabbCenter[0:3])
shapeMesh.rotate([0, 0.5, 0], np.deg2rad(args.roty), aabbCenter[0:3])
shapeMesh.rotate([0, 0, 0.5], np.deg2rad(args.rotz), aabbCenter[0:3])

#copy mesh and translate it so that AABB origin is on system origin and object is on bed
shiftedMesh = copy.deepcopy(shapeMesh)
shiftedMesh.x -= aabbCenter[0]
shiftedMesh.y -= aabbCenter[1]
shiftedMesh.z -= shapeMesh.min_[2] #the lowest z value in the mesh

shiftedCollection = mplot3d.art3d.Poly3DCollection(shiftedMesh.vectors, alpha=0.5)
shiftedCollection.set_facecolor([0.5, 0.5, 0.5])
axes.add_collection3d(shiftedCollection)

#split coordinates of mesh into dimensions then
#remerge as coordinate tuples, then remove duplicates to get set of unique corners
sflatCoords = shiftedMesh.vectors.flatten()
sxs, sys, szs = sflatCoords[0::3], sflatCoords[1::3], sflatCoords[2::3]
corners = set(zip(sxs, sys, szs))
axes.plot(sxs, sys, szs, c=colors.to_rgba("b", 0.2)) # plot lines of mesh translucently
axes.scatter([c[0] for c in corners], [c[1] for c in corners], [c[2] for c in corners], c="r", picker=True)

# Auto scale to the mesh size
scale = shiftedMesh.vectors.flatten(-1)
axes.auto_scale_xyz(scale, scale, scale)

cid = figure.canvas.mpl_connect('pick_event', onPick)

#match corners between model and images in same order
cv2.namedWindow("img")
cv2.setMouseCallback("img", eventCallback)

cv2.imshow("img", calibImg)
cv2.waitKey(1)
pyplot.show()

pickleSave(modelCoords, "model")
pickleSave(imgCoords, "img")
print("Individual pkl files saved")


mapping = {}
for c in modelCoords:
    # TODO check saved format...
    mapping[c] = list(imgCoords.pop(0))

assert len(mapping) == len(modelCoords)

if len(mapping):
    pickleSave(mapping, "mapping")
    print("Saved singleview mapping")
else:
    print("Mapping not saved")

# TODO need undo functions for marking images
