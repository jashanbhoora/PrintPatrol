#!/usr/bin/env python3

import numpy as np
from pyrr import aabb
import cv2
from stl import mesh
from mpl_toolkits import mplot3d
from matplotlib import pyplot, colors
from math import ceil, floor
import copy
import argparse

parser = argparse.ArgumentParser(description='Demonstrates calculation of transform from STL coordinates to Slic3r coordinate space, optionally considering user rotations.')
parser.add_argument('--stl', type=str, help='The mesh to process')
parser.add_argument('--rotx', type=float, help='Rotation around X axis', default=0)
parser.add_argument('--roty', type=float, help='Rotation around Y axis', default=0)
parser.add_argument('--rotz', type=float, help='Rotation around Z axis', default=0)
parser.add_argument('--scale', type=float, help='Apply a scaling factor to the STL in all axes (as a percentage)', default=100)
args = parser.parse_args()

#load mesh
shapeMesh = mesh.Mesh.from_file(args.stl)

# Init figure
figure = pyplot.figure()
axes = mplot3d.Axes3D(figure)
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_zlabel('z')
axes.scatter(0, 0, 0, c="black", marker="D") # origin

#scale mesh
factor = args.scale/100
shapeMesh.x *= factor
shapeMesh.y *= factor
shapeMesh.z *= factor

# Calc AABB
bb = aabb.create_from_points(shapeMesh)
aabbCenter = aabb.centre_point(bb)
axes.scatter(aabbCenter[0], aabbCenter[1], aabbCenter[2], c=colors.to_rgba("r", 0.75)) # AABB center point on original mesh

#apply any rotations
shapeMesh.rotate([0.5, 0, 0], np.deg2rad(args.rotx), aabbCenter[0:3])
shapeMesh.rotate([0, 0.5, 0], np.deg2rad(args.roty), aabbCenter[0:3])
shapeMesh.rotate([0, 0, 0.5], np.deg2rad(args.rotz), aabbCenter[0:3])

flatCoords = shapeMesh.vectors.flatten() # [x00, y00, z00, x01, y02, z03 etc..]
xs, ys, zs = flatCoords[0::3], flatCoords[1::3], flatCoords[2::3] #flat coords for each dimension into their own lists
axes.plot(xs, ys, zs, c=colors.to_rgba("r", 0.2)) # plot lines of mesh translucently

#copy mesh and translate it so that AABB origin is on system origin and object is on bed
shiftedMesh = copy.deepcopy(shapeMesh)
shiftedMesh.x -= aabbCenter[0]
shiftedMesh.y -= aabbCenter[1]
shiftedMesh.z -= shapeMesh.min_[2] #the lowest z value in the mesh

shiftedCollection = mplot3d.art3d.Poly3DCollection(shiftedMesh.vectors, alpha=0.5)
shiftedCollection.set_facecolor([0.5, 0.5, 0.5])
axes.add_collection3d(shiftedCollection)

#split coordinates of mesh into dimensions then
#remerge as coordinate tuples, then remove duplicates to get set of unique corners
sflatCoords = shiftedMesh.vectors.flatten()
sxs, sys, szs = sflatCoords[0::3], sflatCoords[1::3], sflatCoords[2::3]
axes.plot(sxs, sys, szs, c=colors.to_rgba("g", 1.0)) # plot lines of mesh

# plot bed plane as a visual aid
xx, yy = np.meshgrid(range(int(floor(shiftedMesh.min_[0])), int(ceil(shiftedMesh.max_[0]))), range(int(floor(shiftedMesh.min_[1])), int(ceil(shiftedMesh.max_[1]))))
axes.plot_surface(xx, yy, yy*0, color="white", alpha=0.5)

# Auto scale to the mesh size
scale = shiftedMesh.vectors.flatten(-1)
axes.auto_scale_xyz(scale, scale, scale)

pyplot.show()
