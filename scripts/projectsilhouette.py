import pickle
import argparse
import numpy as np
import cv2
from pyrr import aabb
from stl import mesh
import copy
from math import floor, ceil
from threading import Thread
from time import sleep

X = 0
Y = 1

WAIT = 5
RED = (0,0,255)
GREEN = (0,255,0)
BLUE = (255,0,0)
THICKNESS = 1

def bbox(points):
    """
    [xmin ymin]
    [xmax ymax]

    or
    [wmin hmin]
    [wmax hmax]

    """
    a = np.zeros((2,2))
    a[:,0] = np.min(points, axis=0)
    a[:,1] = np.max(points, axis=0)
    return np.transpose(a)

def extractROI(img, bb):
    return img[bb[0][0]:bb[1][0], bb[0][1]:bb[1][1]]

def maskImage(img, mask):
    assert img.shape[:2] == mask.shape[0:2]
    ret = copy.deepcopy(img)
    for h in range(ret.shape[0]):
        for w in range(ret.shape[1]):
            if mask[h, w]:
                ret[h, w] = (255, 255, 255)
            else:
                ret[h, w] = (0, 0, 0)

    return ret

def maskImageGrabCut(img, mask):
    assert img.shape[:2] == mask.shape[:2]
    ret = np.zeros(img.shape[0:2], dtype='uint8')

    k = 2

    for h in range(ret.shape[0]):
        for w in range(ret.shape[1]):
            try:
                if h-k < 0 or h+k > ret.shape[0] or w-k < 0 or w+k > ret.shape[1]:
                    ret[h, w] = cv2.GC_BGD
                else:
                    window = mask[h:h+k, w:w+k] # could fail on edges of image, but should be catered for above
                    mean = window.mean()
                    if mean == 1.0:
                        ret[h, w] = cv2.GC_FGD
                    elif mean > 0.5:
                        ret[h, w] = cv2.GC_PR_FGD
                    elif mean > 0.0:
                        ret[h, w] = cv2.GC_PR_BGD
                    else:
                        ret[h, w] = cv2.GC_BGD
            except:
                #assume edges are definitely background
                print("GC mask exception hit on {},{}".format(h, w))
                ret[h, w] = cv2.GC_BGD
    return ret

def bboxFromSilhouette(silhouette):
    h, w = np.where(np.isin(silhouette, True))
    return bbox(list(zip(h, w))).astype(int)

def computeBarycentricWeights(tri, pixel):
    v1 = tri[0]
    v2 = tri[1]
    v3 = tri[2]
    w1 = ((v2[Y] - v3[Y]) * (pixel[X] - v3[X]) + (v3[X] - v2[X]) * (pixel[Y] - v3[Y]))/((v2[Y] - v3[Y]) * (v1[X] - v3[X]) + (v3[X] - v2[X]) * (v1[Y] - v3[Y]))

    w2 = ((v3[Y] - v1[Y]) * (pixel[X] - v3[X]) + (v1[X] - v3[X]) * (pixel[Y] - v3[Y]))/((v2[Y] - v3[Y]) * (v1[X] - v3[X]) + (v3[X] - v2[X]) * (v1[Y] - v3[Y]))

    w3 = 1 - w1 - w2

    return [w1, w2, w3]

parser = argparse.ArgumentParser(description='Project the silhouette of the STL into the images to locate the model in each one')
parser.add_argument('--stl', type=str, help='The mesh to process')
parser.add_argument('--frontimg', type=str, required=True)
parser.add_argument('--leftimg', type=str, required=True)
parser.add_argument('--rightimg', type=str, required=True)
parser.add_argument('--matrices', type=str, required=True)
parser.add_argument('--saveimgs', action='store_true')
parser.add_argument('--rotx', type=float, help='Rotation around X axis', default=0)
parser.add_argument('--roty', type=float, help='Rotation around Y axis', default=0)
parser.add_argument('--rotz', type=float, help='Rotation around Z axis', default=0)
parser.add_argument('--scale', type=float, help='Apply a scaling factor to the STL in all axes (as a percentage)', default=100)
args = parser.parse_args()

expectedSilhouettes = [[] for i in range(3)]
expectedFullSilhouettes = [[] for i in range(3)]
actualSilhouettes = [[] for i in range(3)]
actualFullSilhouettes = [[] for i in range(3)]

#load mesh and matrices
shapeMesh = mesh.Mesh.from_file(args.stl)
matrices = None
with open(args.matrices, "rb") as f:
    matrices = pickle.load(f)

CAMERA_MATRIX = matrices[3]
imgs = [cv2.imread(args.frontimg), cv2.imread(args.leftimg), cv2.imread(args.rightimg)]
silhouetteMasks = [[] for i in range(3)]
plotImgs = copy.deepcopy(imgs)

#scale mesh
factor = args.scale/100
shapeMesh.x *= factor
shapeMesh.y *= factor
shapeMesh.z *= factor

#center mesh as in slic3r
# Calc AABB
bb = aabb.create_from_points(shapeMesh)
aabbCenter = aabb.centre_point(bb)

#apply any rotations
shapeMesh.rotate([0.5, 0, 0], np.deg2rad(args.rotx), aabbCenter[0:3])
shapeMesh.rotate([0, 0.5, 0], np.deg2rad(args.roty), aabbCenter[0:3])
shapeMesh.rotate([0, 0, 0.5], np.deg2rad(args.rotz), aabbCenter[0:3])

#translate mesh
shapeMesh.x -= aabbCenter[0]
shapeMesh.y -= aabbCenter[1]
shapeMesh.z -= shapeMesh.min_[2] #the lowest z value in the mesh

threads = []
progress = [0.0, 0.0, 0.0]

def getProgress():
    while sum(progress)/3 < 100:
        print("Progress: Front={:.2f}%, Left={:.2f}%, Right={:.2f}%, total={:.2f}%".format(*progress, sum(progress)/3))
        sleep(0.5)

def projectPoints(idx, img, r, t, meshVecs, camMat, silMasks, expSils, expFullSils):
    global progress
    silMasks[idx] = np.zeros((img.shape[0], img.shape[1]), dtype=bool)
    for triNo, triangle in enumerate(meshVecs):
        #print("Viewpoint {}/{}, triangle {}/{}".format(idx+1, len(imgs), triNo+1, len(meshVecs)))
        progress[idx] = ((triNo+1) / len(meshVecs)) * 100
        triProj = cv2.projectPoints(triangle, r, t, camMat, None)[0]
        triProj = np.array([x[0] for x in triProj]) # forms into list of 3 image points
        bb = bbox(triProj) # bounding box of triangle in image

        for w in range(floor(bb[0][0]), ceil(bb[1][0])):
            for h in range(floor(bb[0][1]), ceil(bb[1][1])):
                weights = computeBarycentricWeights(triProj, (w,h))
                ## TODO check whether this pixel is above the current z height/slice that is currently being printed.
                ## If it is, it can be ignored. If it is below the current slice height and instead the triangle, then set True in mask.
                if list(map(abs, weights)) == weights:
                    # pixel is inside triangle
                    silMasks[idx][h][w] = True
                    plotImgs[idx][h][w] = (0, 255, 0)

    _, expSil = cv2.threshold(silMasks[idx].astype("uint8"), 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    expSils[idx] = extractROI(expSil, bboxFromSilhouette(silMasks[idx]))
    expFullSils[idx] = expSil  

threadProgress = Thread(target=getProgress)
threadProgress.start()
for idx, img in enumerate(imgs):
    r, t = matrices[idx]
    process = Thread(target=projectPoints, args=[idx, img, r, t, shapeMesh.vectors, CAMERA_MATRIX, silhouetteMasks, expectedSilhouettes, expectedFullSilhouettes])
    process.start()
    threads.append(process)

for process in threads:
    process.join()

if args.saveimgs:
    cv2.imwrite("front_silhouette.bmp", plotImgs[0])
    cv2.imwrite("left_silhouette.bmp", plotImgs[1])
    cv2.imwrite("right_silhouette.bmp", plotImgs[2])
    print("Images saved")

# Now have the predicted silhouettes given STL and camera model
# Need to extract actual silhouette of printed model from image

def maskAndSave(idx, img):
    maskedImg = maskImage(img, silhouetteMasks[idx]).astype('uint8')  
    #cv2.imwrite("mask{}.bmp".format(idx), maskedImg)
    greyImg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurGreyImg = cv2.GaussianBlur(greyImg,(3,3),0)
    silBb = bboxFromSilhouette(silhouetteMasks[idx])

    #grabcut
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)
    rect = (silBb[0][0],silBb[0][1],silBb[1][0],silBb[1][1])
    print("GC {} masking".format(idx))
    maskedImgGC = maskImageGrabCut(img, silhouetteMasks[idx])
    print("GC {}".format(idx))
    maskGCResult, fgMod, bgMod = cv2.grabCut(img,maskedImgGC,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
    maskGCResult = np.where((maskGCResult==2)|(maskGCResult==0),0,1).astype('uint8')

    #convert to black/white image for silhouette consistency checking
    _, gcSilhouette = cv2.threshold(maskGCResult, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    actualSilhouettes[idx] = extractROI(gcSilhouette, silBb)
    actualFullSilhouettes[idx] = gcSilhouette

maskThreads = []

for idx, img in enumerate(imgs):
    proc = Thread(target=maskAndSave, args=[idx, img])
    proc.start()
    maskThreads.append(proc)

for proc in maskThreads:
    proc.join()

import code
code.interact(local=locals())

# save silhouettes
f = open("croppedSilhouettes.pkl", "wb")
pickle.dump([expectedSilhouettes, actualSilhouettes], f)
f.close()

f = open("fullSilhouettes.pkl", "wb")
pickle.dump([expectedFullSilhouettes, actualFullSilhouettes], f)
f.close()
print("Silhouettes pickled")
# import code
# code.interact(local=locals())
