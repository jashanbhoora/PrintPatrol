import argparse
import numpy as np
from mpl_toolkits import mplot3d
import pickle
import cv2

parser = argparse.ArgumentParser(description='Plots points from pickled mapping against images.')
parser.add_argument('mapping', type=str, help='The pickled file')
parser.add_argument('front', type=str)
parser.add_argument('left', type=str)
parser.add_argument('right', type=str)

args = parser.parse_args()
f = open(args.mapping, "rb")
mapping = pickle.load(f)

imgs = [cv2.imread(args.front), cv2.imread(args.left), cv2.imread(args.right)]

for count, v in enumerate(mapping.values()):
    for i, c in enumerate(v):
        if c[0] != None:
            cv2.putText(imgs[i], "{}".format(count), (c[0], c[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.circle(imgs[i], (c[0],c[1]), 2, (0, 255, 0), -1)
        else:
            print("{} is not visible".format(count))

for idx, i in enumerate(imgs):
    cv2.imshow("img {}".format(idx), i)
    cv2.waitKey(100)

import code
code.interact(local=locals())
