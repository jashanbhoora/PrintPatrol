
import numpy as np
import cv2
import glob
import sys

SIZE = (3,6)
SQUARE_SIZE = 10 #mm

if sys.argv[1] == "record":
    import subprocess
    from time import time
    FOCUS = 30
    print("Disabling autofocus and setting focus to {} on first video device".format(FOCUS)) # note that the system needs to have v4l-utils installed for this
    for c in sys.argv[2]:
        subprocess.call(["v4l2-ctl -d {} -c focus_auto=0 --verbose".format(c)], shell=True)
        subprocess.call(["v4l2-ctl -d {} -c focus_absolute={} --verbose".format(c, FOCUS)], shell=True)

    cap = cv2.VideoCapture(int(sys.argv[2]))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

    cv2.waitKey(100)

    # Check if camera opened successfully
    if (cap.isOpened() == False): 
        print("Unable to read camera feed")
    
    # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.    
    while(True):
        ret, frame = cap.read()
        if ret: 
            # Display the resulting frame    
            cv2.imshow('frame',frame)
            pressed = cv2.waitKey(1) & 0xFF
            if pressed == ord("c"):
                print("capturing image")
                cv2.imwrite("data/calib_{}.bmp".format(time()), frame)
            # Press Q on keyboard to stop recording
            if pressed == ord('q'):
                break
        # Break the loop
        else:
            print("read loop error")
            break 
    
    # When everything done, release the video capture and video write objects
    cap.release()
    exit()

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
#objp = np.zeros((SIZE[0]*SIZE[1],3), np.float32)
#objp[:,:2] = np.mgrid[0:SIZE[1]*SQUARE_SIZE:SQUARE_SIZE,0:SIZE[0]*SQUARE_SIZE:SQUARE_SIZE].T.reshape(-1,2)
objp = np.zeros((SIZE[0]*SIZE[1],3), np.float32)
objp[:,:2] = np.mgrid[0:SIZE[1]*SQUARE_SIZE:SQUARE_SIZE, 0:SIZE[0]*SQUARE_SIZE:SQUARE_SIZE].T.reshape(-1,2)
print(objp)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('../datasets/checkerboard_c920/*.bmp')
#images = glob.glob('D:/OneDrive/Documents/University/Final Year/Project/calibimg/*.jpg')

for fname in images:
    print(fname)
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, SIZE[::-1], None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(img, SIZE[::-1], corners2, ret)
        # cv2.imshow('img', img)
        # cv2.waitKey(500)

print("calibrating...")
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
print("calibration done")
#print("Camera Matrix:\n{0}\n{1}\n{2}".format(str(mtx), str(rvecs), str(tvecs)))
print("Camera Matrix:\n{}".format(str(mtx)))
print("Dist: {}".format(dist))

# for fname in images:
#     # undistort
#     img = cv2.imread(fname)
#     dst = cv2.undistort(img, mtx, dist)
#     #cv2.imshow('img',img)
#     #cv2.waitKey(5500)
#     cv2.imwrite("udist_{0}.jpg".format(fname), dst)

R, _ = cv2.Rodrigues(rvecs[0])

#print("Rodrigues:\n{0}".format(str(R)))

mean_error = 0
tot_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
    error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
    tot_error += error

print("total error: {0}".format(mean_error/len(objpoints)))

cv2.destroyAllWindows()
