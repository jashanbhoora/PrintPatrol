#!/usr/bin/env python3

import numpy as np
import cv2
import argparse
import subprocess
import copy

# TODO improve argument format
# TODO add scaling transforms?

parser = argparse.ArgumentParser(description='Capture focussed imgs for camcal offline')
parser.add_argument('frontcam', type=int, help='The index of the front camera')
parser.add_argument('leftcam', type=int, help='The index of the left camera')
parser.add_argument('rightcam', type=int, help='The index of the right camera')
parser.add_argument('outfile', type=str, help='Name of the output file')

args = parser.parse_args()

LEFT = 1
MIDDLE = 2
RIGHT = 3
FOCUS = 30

FRONTCAM = args.frontcam # camera facing the front of the printer
LEFTCAM = args.leftcam # first camera moving clockwise from the front camera around the printer
RIGHTCAM = args.rightcam # the remaining camera
CAMNAMES = {FRONTCAM: "front", LEFTCAM: "left", RIGHTCAM: "right"} # just for printing

calibImgs = {}
DELAY = 200


# TODO use v4l python bindings instead
print("Disabling autofocus and setting focus to {} on first 3 video devices".format(FOCUS)) # note that the system needs to have v4l-utils installed for this
for c in [0, 1, 2]:
    subprocess.call(["v4l2-ctl -d {} -c focus_auto=0 --verbose".format(c)], shell=True)
    subprocess.call(["v4l2-ctl -d {} -c focus_absolute={} --verbose".format(c, FOCUS)], shell=True)

if True:
    for c in [FRONTCAM, LEFTCAM, RIGHTCAM]:
        cap = cv2.VideoCapture(c)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        while True:
            # Capture frame-by-frame
            ret, frame = cap.read()
            cv2.imshow('frame', frame)
            pressed = cv2.waitKey(1) & 0xFF
            if pressed == ord('q'):
                if True:
                    cv2.imwrite("capture_{}_{}.bmp".format(args.outfile, CAMNAMES[c]), frame)
                break
        # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()
    exit()
