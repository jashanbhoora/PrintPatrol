This repository contains the core of a system to detect the errors of an open source (RepRap) 3D printer in real-time. It is the result of my Masters project whilst studying Computer Science with Embedded Systems at the University of York.
I am currently writing a blog post that will describe the system more completely. Until then, this source is available in standard RepRap fashion.
Once I am in a suitable position to do so, I hope to finish this implementation and properly document it for use, so stay tuned!

I have published a blog post that contains a very condensed explanation of how the system works, so feel free to take a look: www.jashanbhoora.co.uk/blog/print-patrol